import { Request, Response } from 'express';
import * as FCM from 'fcm-node';
import * as moment from 'moment';
import * as request from 'request';
import { EntityManager, Equal, MoreThan, Not } from 'typeorm';
import { ParkingLot } from '../entities/ParkingLot';
import { Transaction, TransactionState } from '../entities/Transaction';
import { User } from '../entities/User';
import { Vehicle } from '../entities/Vehicle';
import HttpError from '../exceptions/HttpException';
import { authReq } from '../interfaces/token.interface';
import { asyncRunner } from '../utils/async';

const serverKey =
    'AAAAomRGM0M:APA91bEPbPLwiNQfUDVqM00T8RelhHwvwoWnuktduA6DbfSYt1cJmhKRvdiwubvfL9ESfjpgl1l26-3AjmnOoxEeTc40-T3u3i194cawVHvABsruztj0GgmgajhwhqhEzvpJa-OXZntq';

const fcm = new FCM(serverKey);

const sendNotification = (
    message: { to: string; notification: { [key: string]: any }; data: { [key: string]: any } },
    transaction: Transaction,
    isFlutter: boolean,
) => {
    if (isFlutter) {
        transaction = { ...transaction };
        delete transaction.parkingLot;
        delete transaction.user;
        const body = {
            token: message.to,
            payload: {
                notification: { ...message.notification, click_action: 'FLUTTER_NOTIFICATION_CLICK' },
                data: { transaction: JSON.stringify(transaction) },
            },
        };
        request.post(
            {
                url: 'https://us-central1-webrtc-test-deb99.cloudfunctions.net/sendNotification2',
                headers: { 'Content-Type': 'text/plain' },
                body: JSON.stringify(body), 
            },
            (err, httpResponse, body) => {
                if (err) console.error(err);
                // console.log(httpResponse);
                // console.log(body);
            },
        );
    } else {
        fcm.send(message, function (err, response) {
            if (err) {
                console.log(err);
                console.log(response);
                console.log('Firebase Error');
            } else {
                console.log('Successfully sent with response: ', response);
            }
        });
    }
};

const getPlate = async (plateImg) => {
    return new Promise<string>((resolve, reject) => {
        request.post(
            {
                url: 'https://api.platerecognizer.com/v1/plate-reader/',
                formData: { upload: { value: plateImg.buffer, options: { filename: 'img' } } },
                headers: { Authorization: 'token acb6939f8b45efcf200be028ab2efde95c16c474' },
            },
            (err, httpResponse, body) => {
                if (err) console.error(err);
                else {
                    const response = JSON.parse(body);
                    const plate = response.results[0].plate;
                    resolve(plate.replace(/\s/g, '').toUpperCase());
                }
            },
        );
    });
};

export class TransactionsService {
    getActiveTransactions = async (req: authReq, res: Response) =>
        asyncRunner(req, res, async (req: authReq, res: Response, db) => {
            const userId = req.decodedToken?._id;
            if (!userId) return res.status(401).send('Unauthorized');

            const transaction = await db
                .createQueryBuilder(Transaction, 't')
                .innerJoinAndSelect('t.parkingLot', 'pl')
                .andWhere('t.user_id = :userId', { userId })
                .andWhere('t.state = :active', { active: TransactionState.ACTIVE })
                .getOne();

            res.send({ transaction: transaction || null });
        });

    createTransaction = async (req: authReq, res: Response) => {
        return asyncRunner(req, res, async (req: Request, res: Response, db: EntityManager) => {
            const { parkingLotId, plate, os, alone } = req.body;
            const files = (req as any).files;
            const plateImg = files && files.length > 0 ? files[0] : undefined;

            if (!((plateImg || plate) && parkingLotId)) {
                throw new HttpError(400, 'Missing required fields plateImg and parkingLotId ');
            }

            const parkingLot = await db.findOne(ParkingLot, parkingLotId);

            let vehiclePlate: string;
            if (!plate) {
                vehiclePlate = await getPlate(plateImg);
            } else {
                vehiclePlate = plate;
            }
            let _os: string;
            if (!os) {
                _os = 'UNKNOWN';
            } else {
                _os = os;
            }

            const vehicles = await db.find(Vehicle, { where: { plate: vehiclePlate }, relations: ['user'] });

            vehicles.forEach(async (v) => {
                let transaction = new Transaction(new Date(), v.plate, v.user.id, parkingLotId, _os, alone ?? false);
                transaction = await db.save(transaction);
                if (v.user.fcmToken) {
                    const message = {
                        to: v.user.fcmToken,
                        notification: {
                            title: 'New Parking Registered',
                            body: 'Tap to accept or reject the transaction',
                            click_action: 'start_transaction',
                        },
                        data: {
                            type: 'NEW_TRANSACTION',
                            transactionId: transaction.id,
                            plate: v.plate,
                            parkingLotName: parkingLot.name,
                            startTime: transaction.startTime,
                        },
                    };

                    sendNotification(message, transaction, v.user.isFlutter);
                }
            });

            return res.status(201).end();
        });
    };

    updateTransaction = async (req: authReq, res: Response) => {
        const userId = req.decodedToken?._id;
        if (!userId) return res.status(401).send('Unauthorized');

        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            //@ts-ignore
            const id = req.params.id;
            if (!id) {
                throw new HttpError(400, 'Missing transaction id in url parameter');
            }
            //@ts-ignore
            const { state } = req.body;
            if (state === undefined) {
                throw new HttpError(400, 'Missing required parameter state');
            } else if (state != TransactionState.ACTIVE && state != TransactionState.REJECTED) {
                throw new HttpError(400, 'Transaction state should be ACTIVE or REJECTED');
            }

            const transaction = await db.findOne(Transaction, { where: { id, user_id: userId } });
            if (!transaction) {
                throw new HttpError(404, `Transaction with id ${id} not found`);
            }
            if (transaction.state === TransactionState.COMPLETED) {
                throw new HttpError(400, 'Transaction state is already completed');
            }
            transaction.state = state;
            let resultTransaction = await db.save(transaction);
            res.status(200).send(resultTransaction);
        });
    };

    getTransactions = async (req: authReq, res: Response) => {
        const userId = req.decodedToken?._id;
        if (!userId) return res.status(401).send('Unauthorized');

        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            let ifModified: Date | undefined = undefined;
            //@ts-ignore
            if ('if-modified-since' in req.headers) ifModified = new Date(Date.parse(req.headers['if-modified-since']));

            const filter = { user_id: userId, state: Not(Equal(TransactionState.REJECTED)) };

            let transactions = await db.find(Transaction, {
                where: ifModified !== undefined ? { ...filter, updatedAt: MoreThan(ifModified) } : filter,
                relations: ['parkingLot'],
            });

            const lastUpdate = transactions.reduce((p, c) => {
                if (p === null || c.updatedAt > p) p = c.updatedAt;
                return p;
            }, null as null | Date);
            if (lastUpdate !== null) {
                res.setHeader('modified-at', lastUpdate.toISOString());
                res.setHeader('access-control-expose-headers', 'modified-at');
            }

            res.status(200).send(transactions);
        });
    };

    completeTransaction = async (req: Request, res: Response) => {
        return asyncRunner(req, res, async (req: Request, res: Response, db: EntityManager) => {
            const { parkingLotId, plate } = req.body;
            const files = (req as any).files;
            const plateImg = files && files.length > 0 ? files[0] : undefined;

            if (!((plateImg || plate) && parkingLotId)) {
                throw new HttpError(400, 'Missing required fields plateImg and parkingLotId ');
            }

            let vehiclePlate: string;
            if (!plate) {
                vehiclePlate = await getPlate(plateImg);
            } else {
                vehiclePlate = plate;
            }

            let transaction = await db
                .createQueryBuilder(Transaction, 't')
                .innerJoinAndSelect('t.parkingLot', 'p')
                .andWhere('t.state = :active', { active: TransactionState.ACTIVE })
                .andWhere('t.vehicle_plate = :plate', { plate: vehiclePlate })
                .andWhere('t.parking_lot_id = :parkingLotId', { parkingLotId })
                .orderBy('t.created_at', 'DESC')
                .getOne();

            if (!transaction) {
                throw new HttpError(404, 'Active transaction does not exist');
            }

            transaction.user = await db.findOne(User, transaction.user_id);

            transaction.state = TransactionState.COMPLETED;
            transaction.endTime = new Date();
            const duration = moment.duration(moment(transaction.endTime).diff(transaction.startTime));
            transaction.cost = Math.floor(300 * duration.asMinutes());

            transaction = await db.save(transaction);

            const message = {
                to: transaction.user.fcmToken,
                notification: {
                    title: 'Transaction complete',
                    body:
                        `Location: ${transaction.parkingLot.name}\n` +
                        `Duration: ${duration.asMinutes()} minutes\n` +
                        `Cost: ${transaction.cost}\n`,
                    click_action: 'view_transactions',
                },
                data: {
                    type: 'TRANSACTION_COMPLETE',
                    transactionId: transaction.id,
                    plate: transaction.vehicle_plate,
                    parkingLotName: transaction.parkingLot.name,
                    cost: transaction.cost,
                },
            };

            sendNotification(message, transaction, transaction.user.isFlutter);

            res.status(200).send(transaction);
        });
    };
}
