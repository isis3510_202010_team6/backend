import { Request, Response } from 'express';
import { EntityManager, Not } from 'typeorm';
import { Vehicle, VehicleState } from '../entities/Vehicle';
import HttpError from '../exceptions/HttpException';
import { authReq } from '../interfaces/token.interface';
import { asyncRunner } from '../utils/async';
import { filterUndefined } from '../utils/other';

export default class VehiclesService {
    getVehicles = async (req: authReq, res: Response) => {
        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            const userId = req.decodedToken?._id;
            if (!userId) return res.status(401).send('Unauthorized');

            const vehicles = await db.find(Vehicle, { where: { user_id: userId, state: Not(VehicleState.DELETED) } });
            res.send(vehicles);
        });
    };

    setActive = async (req: authReq, res: Response) => {
        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            const userId = req.decodedToken?._id;
            if (!userId) return res.status(401).send('Unauthorized');
            //@ts-ignore
            const { plate, inactive } = req.body;
            if (!plate) throw new HttpError(400, 'Missing plate');

            await db
                .createQueryBuilder()
                .update(Vehicle)
                .set({ state: inactive === true ? VehicleState.INACTIVE : VehicleState.ACTIVE })
                .where('user_id = :userId', { userId })
                .andWhere('state != :deleted', { deleted: VehicleState.DELETED })
                .andWhere('plate = :plate', { plate })
                .execute();

            res.status(200).end();
        });
    };

    createVehicle = async (req: authReq, res: Response) => {
        const userId = req.decodedToken?._id;
        if (!userId) return res.status(401).send('Unauthorized');

        return asyncRunner(req, res, async (req: Request, res: Response, db: EntityManager) => {
            const { plate, description, state, type } = req.body;
            if (!(plate && description)) {
                throw new HttpError(400, 'Missing required fields plate and description');
            }

            // await db
            //     .createQueryBuilder()
            //     .update(Vehicle)
            //     .set({ state: VehicleState.INACTIVE })
            //     .where('user_id = :userId', { userId })
            //     .andWhere('state != :deleted', { deleted: VehicleState.DELETED })
            //     .execute();

            let newVehicle = new Vehicle(
                plate.replace(/\s/g, '').toUpperCase(),
                userId,
                description,
                type !== undefined ? type : 'SMALL',
            );
            if (state !== undefined) {
                newVehicle.state = state;
            }
            newVehicle = await db.save(newVehicle);

            res.status(201).send(newVehicle);
        });
    };

    updateVehicle = async (req: authReq, res: Response) => {
        const userId = req.decodedToken?._id;
        if (!userId) return res.status(401).send('Unauthorized');

        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            //@ts-ignore
            const plate = req.params.plate;
            if (!plate) {
                throw new HttpError(400, 'Missing vehicle plate in url parameter');
            }
            //@ts-ignore
            const { state, description } = req.body;
            if (state === undefined && description === undefined) {
                throw new HttpError(400, 'Missing required active plate or description');
            }

            const v = await db.update(Vehicle, { plate, user_id: userId }, filterUndefined({ state, description }));
            if (v.affected == 1) res.status(200).send();
            else throw new HttpError(404, `A vehicle with the plate ${plate} doesn't exists`);
        });
    };

    deleteVehicle = async (req: authReq, res: Response) => {
        const userId = req.decodedToken?._id;
        if (!userId) return res.status(401).send('Unauthorized');

        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            //@ts-ignore
            const plate = req.params.plate;
            if (!plate) {
                throw new HttpError(400, 'Missing vehicle plate in url parameter');
            }

            const v = await db.update(Vehicle, { plate, user_id: userId }, { state: VehicleState.DELETED });
            if (v.affected == 1) res.status(200).send();
            else throw new HttpError(404, `A vehicle with the plate ${plate} doesn't exists`);

            res.status(200).send();
        });
    };
}
