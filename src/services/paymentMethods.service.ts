import { Request, Response } from 'express';
import { EntityManager } from 'typeorm';
import { PaymentMethod } from '../entities/PaymentMethod';
import HttpError from '../exceptions/HttpException';
import { authReq } from '../interfaces/token.interface';
import { asyncRunner } from '../utils/async';

export default class PaymentMethodsService {
    createPaymentMethod = async (req: authReq, res: Response) => {
        const userId = req.decodedToken?._id;
        if (!userId) return res.status(401).send('Unauthorized');

        return asyncRunner(req, res, async (req: Request, res: Response, db: EntityManager) => {
            const { provider, lastDigits, description } = req.body;
            if (!(provider && lastDigits)) {
                throw new HttpError(400, 'Missing required fields provider and latsDigits');
            }

            let value = new PaymentMethod(lastDigits, provider, userId, description);
            value = await db.save(value);

            res.status(201).send(value);
        });
    };

    deletePaymentMethod = async (req: authReq, res: Response) => {
        const userId = req.decodedToken?._id;
        if (!userId) return res.status(401).send('Unauthorized');

        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            //@ts-ignore
            const id = req.params.id;
            if (!id) {
                throw new HttpError(400, 'Missing vehicle id in url parameter');
            }

            const v = await db.delete(PaymentMethod, { id, user_id: userId });
            if (v.affected == 1) res.status(200).send();
            else throw new HttpError(404, `The payment method doesn't exists`);
        });
    };
}
