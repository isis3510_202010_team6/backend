import { Response } from 'express';
import { EntityManager } from 'typeorm';
import { ParkingLot } from '../entities/ParkingLot';
import HttpError from '../exceptions/HttpException';
import { authReq } from '../interfaces/token.interface';
import { asyncRunner } from '../utils/async';

export class ParkingLotsService {
    getParkingLots = async (req: authReq, res: Response) => {
        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            let parkingLots = await db.find(ParkingLot, {});
            res.status(200).send(parkingLots);
        });
    };

    getParkingLot = async (req: authReq, res: Response) => {
        return asyncRunner(req, res, async (req: authReq, res: Response, db: EntityManager) => {
            const id = req.params.id;
            if (!id) {
                throw new HttpError(400, 'Missing parking lot id in url parameter');
            }

            let parkingLot = await db.findOne(ParkingLot, { where: { id } });
            res.status(200).send(parkingLot);
        });
    };
}
