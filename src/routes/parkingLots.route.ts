import { Router } from 'express';
import Route from '../interfaces/routes.interface';
import { ParkingLotsService } from '../services/parkingLots.service';

export class ParkingLotsRoute implements Route {
    public path = '/parking-lots';
    public router = Router();
    public service = new ParkingLotsService();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(this.path, this.service.getParkingLots);
        this.router.get(`${this.path}/:id`, this.service.getParkingLot);
    }
}
