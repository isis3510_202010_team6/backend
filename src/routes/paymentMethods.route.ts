import { Router } from 'express';
import Route from '../interfaces/routes.interface';
import PaymentMethodsService from '../services/paymentMethods.service';

export class PaymentMethodsRoute implements Route {
    public path = '/payment-methods';
    public router = Router();
    public paymentMethodsService = new PaymentMethodsService();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(this.path, this.paymentMethodsService.createPaymentMethod);
        this.router.delete(`${this.path}/:id`, this.paymentMethodsService.deletePaymentMethod);
    }
}
