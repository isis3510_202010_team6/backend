import { Router } from 'express';
import Route from '../interfaces/routes.interface';
import VehiclesService from '../services/vehicles.service';
import { throws } from 'assert';

export class VehiclesRoute implements Route {
    public path = '/vehicles';
    public router = Router();
    public vehiclesService = new VehiclesService();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(this.path, this.vehiclesService.createVehicle);
        this.router.get(this.path, this.vehiclesService.getVehicles)
        this.router.post(`${this.path}/active`, this.vehiclesService.setActive)
        this.router.put(`${this.path}/:plate`, this.vehiclesService.updateVehicle);
        this.router.delete(`${this.path}/:plate`, this.vehiclesService.deleteVehicle);
    }
}
