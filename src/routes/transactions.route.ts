import { Router } from 'express';
import Route from '../interfaces/routes.interface';
import { TransactionsService } from '../services/transactions.service';
import * as multer from 'multer';

export class TransactionsRoute implements Route {
    public path = '/transactions';
    public router = Router();
    public transactionsService = new TransactionsService();
    getFields = multer();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(this.path, this.getFields.any() as any, this.transactionsService.createTransaction);
        this.router.get(this.path, this.transactionsService.getTransactions);
        this.router.get(`${this.path}/active`, this.transactionsService.getActiveTransactions);
        this.router.put(`${this.path}/:id`, this.transactionsService.updateTransaction);
        this.router.post(
            `${this.path}/complete`,
            this.getFields.any() as any,
            this.transactionsService.completeTransaction,
        );
    }
}
