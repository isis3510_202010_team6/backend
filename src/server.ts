import 'reflect-metadata';
import App from './app';
import { ParkingLotsRoute } from './routes/parkingLots.route';
import { PaymentMethodsRoute } from './routes/paymentMethods.route';
import { TransactionsRoute } from './routes/transactions.route';
import { UsersRoute } from './routes/users.route';
import { VehiclesRoute } from './routes/vehicles.route';

const app = new App([
    new UsersRoute(),
    new VehiclesRoute(),
    new PaymentMethodsRoute(),
    new TransactionsRoute(),
    new ParkingLotsRoute(),
]);

app.listen();
