import { Chance } from 'chance';
import * as moment from 'moment';
import { EntityManager } from 'typeorm';
import { ConcurrentUsers } from '../entities/ConcurrentUsers';
import { DistanceTravelled } from '../entities/DistanceTravelled';
import { Employee, ShiftType } from '../entities/Employee';
import { ParkingLot } from '../entities/ParkingLot';
import { Transaction, TransactionState } from '../entities/Transaction';
import { User } from '../entities/User';
import { Vehicle } from '../entities/Vehicle';
import { hashPassword } from './encryption';

const chance = new Chance.Chance();

export const getMockParkingLots = () => {
    const p1 = new ParkingLot(
        'National Park',
        'Sed culpa consequuntur labore in. Quis quia recusandae amet. Consectetur doloribus sit omnis temporibus officia. Earum ipsum tempora occaecati fugit. Deserunt facilis autem occaecati consequatur iure maxime ut.',
        4.669515485820514,
        -74.05895933919157,
        'Calle 45 # 7 - 12',
    );
    p1.id = '0ca817ee-b739-41fc-9493-c9c8dc75ee99';

    const p2 = new ParkingLot(
        'Local Park',
        'Qui ratione officiis repellat. Et maiores facilis optio excepturi animi. Ut consequatur consequatur non omnis. Omnis ut ad enim quia in sit. Facere temporibus ipsam nesciunt recusandae ex qui dolores eos.',
        4.610515485820514,
        -74.07895933919157,
        'Calle 80 # 11 - 38',
    );
    p2.id = '0ca812ee-b739-41fc-9493-c9c9dc75ee99';

    const p3 = new ParkingLot(
        'Virrey Park',
        'Voluptas placeat quia itaque consequatur reprehenderit sunt ipsa eligendi. Sed est pariatur consequatur voluptas sunt omnis non numquam veritatis. Praesentium est molestiae aut et. Quos nobis dolor enim est.',
        4.607683967477323,
        -73.9008203466492,
        'Carrera 15 # 87 - 67',
    );
    p3.id = '0ca817ae-b739-41fc-9493-c9c9dc75ee99';

    const p4 = new ParkingLot(
        'Tequendama',
        'Voluptas placeat quia itaque consequatur reprehenderit sunt ipsa eligendi. Sed est pariatur consequatur voluptas sunt omnis non numquam veritatis. Praesentium est molestiae aut et. Quos nobis dolor enim est.',
        4.615683967477323,
        -74.0708203466492,
        'Carrera 15 # 87 - 67',
    );
    p4.id = '1ca817ae-b739-41fc-9493-c9c9dc75ee99';

    const p5 = new ParkingLot(
        'Andres',
        'Voluptas placeat quia itaque consequatur reprehenderit sunt ipsa eligendi. Sed est pariatur consequatur voluptas sunt omnis non numquam veritatis. Praesentium est molestiae aut et. Quos nobis dolor enim est.',
        4.585683967477323,
        -74.1708203466492,
        'Carrera 15 # 87 - 67',
    );
    p5.id = '2ca817ae-b739-41fc-9493-c9c9dc75ee99';

    return [p1, p2, p3, p4, p5];
};

const _VehicleDesc = ['Toyota', 'Nissan', 'Hyundai', 'Chevrolet'];
const _VehicleType = ['Big', 'Small'];
const _OSTypes = ['IOS', 'ANDROID'];
const _Addresses = [
    'Carrera 4 # 16 - 40',
    'Carrera 19 # 4 - 80',
    'Transversal 1 # 30 - 90',
    'Avenida 19 # 34 - 90',
    'Carrera 190 # 10 - 9',
    'Avenida 76 # 30 - 98',
];
const _JobTypes = [
    'Construction',
    'Lawyer',
    'Doctor',
    'Fireman',
    'Student',
    'Student',
    'Lawyer',
    'Developer',
    'Developer',
    'Banking',
    'Painter',
    'Student',
];

const distances = [1000, 2300, 5000, 5000, 5000, 1200, 500, 500, 500, 500, 200, 200, 200];

const LatLong = [
    '4.33.223, 3.45.636',
    '7.33.663, 3.45.636',
    '5.33.663, 3.45.636',
    '42.33.25623, 3.45.636',
    '34.63.223, 3.45.636',
    '14.83.523, 3.45.636',
    '4.39.3, 3.45.636',
    '4.03.23, 3.45.636',
    '4.13.23, 3.45.636',
];
const _ShiftType = ['DAY', 'NIGHT'];

const _Names = ['Juan', 'Alberto', 'Jimenez', 'Alvaro', 'Benjamin'];

const _Economic = ['Estrato 1', 'Estrato 2', 'Estrato 3', 'Estrato 4', 'Estrato 5', 'Estrato 6'];

const _CarTypes = [
    'Suv',
    'Pickup',
    'Sedan',
    'Pickup',
    'Sedan',
    'Pickup',
    'Sedan',
    'Pickup',
    'Sedan',
    'Pickup',
    'Sedan',
    'Pickup',
    'Sedan',
    'Suv',
    'Sedan',
    'Suv',
    'Suv',
    'Suv',
];

export const randomClientInfo = () => {
    const job_type = _JobTypes[randUniform(0, _JobTypes.length)];
    const economicStanding = _Economic[randUniform(0, 6)];
    const numberOfCars = randUniform(0, 4);
    const age = randUniform(17, 65);
    const alone = Math.round(Math.random() * 6) % 1 === 0 ? false : true;
    return new ClientInfo(job_type, economicStanding, numberOfCars, age, alone);
};

export const randomEmployee = () => {
    const name = _Names[randUniform(0, 5)];
    const assignedParkingLot = getMockParkingLots()[randUniform(0, 4)].name;
    const shiftType = _ShiftType[randUniform(0, 1)];
    return new Employee(name, assignedParkingLot, ShiftType[shiftType]);
};

export const randomConcurrentUsers = (days) => {
    const startdate = '2020/02/06';
    const timestamp = moment(startdate, 'YYYY/MM/DD').add(days, 'days');
    const parkingLot = getMockParkingLots()[randUniform(0, 4)].name;
    const amountOfUsers = randUniform(1, 50);
    return new ConcurrentUsers(timestamp.format('MM/DD'), parkingLot, amountOfUsers);
};

export const randomDistanceTravelled = () => {
    let id = randUniform(3000, 90000) + '';
    const distance = randUniform(1000, 34000);
    const parkinglot = getMockParkingLots()[randUniform(0, 4)].name;

    return new DistanceTravelled(distance, null, parkinglot);
};

export const randomVehicle = (userId: string): Vehicle => {
    let ss = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let plate = '';
    let type = Math.floor(Math.random() * 1);
    [...Array(6)].map((_, i) => {
        if (i === 3) {
            ss = '0123456789';
            plate += ' ';
        }
        plate += ss.charAt(randUniform(0, ss.length - 1));
    });
    return new Vehicle(plate, userId, _VehicleDesc[randUniform(0, _VehicleDesc.length - 1)], _VehicleType[type]);
};

export const getMockData = async (db: EntityManager) => {
    const allConcurrentUsers: ConcurrentUsers[] = [];
    const allDistanceTravelled: DistanceTravelled[] = [];
    const allEmployees: Employee[] = [];
    const allClientInfo: ClientInfo[] = [];

    let i = 0;
    while (++i < 200) {
        allConcurrentUsers.push(randomConcurrentUsers(i));
        allDistanceTravelled.push(randomDistanceTravelled());
        allEmployees.push(randomEmployee());
        allClientInfo.push(randomClientInfo());
    }
    await db.save(allConcurrentUsers).catch((err) => console.log(err, 'CONCURRENT'));
    await db.save(allDistanceTravelled).catch((err) => console.log(err, 'DISATNCe'));
    await db.save(allEmployees).catch((err) => console.log(err, 'EMPLOYEES'));
    await db.save(allClientInfo).catch((err) => console.log(err, 'CLIENT'));
};

export const getMockTransactions = async (db: EntityManager) => {
    const numUsers = 100;
    const alone = Math.round(Math.random() * 6) % 1 === 0 ? false : true;
    const allCars: Vehicle[] = [];
    const allUsers: User[] = [];
    const allTransactions: Transaction[] = [];
    const parkingLots = await db.find(ParkingLot);

    const password = await hashPassword('000000');
    for (let i = 0; i < numUsers; i++) {
        const user = new User(chance.name(), chance.email(), password, chance.phone(), LatLong[randUniform(0, 6)]);
        allUsers.push(user);
        const vehicleType = Math.floor(Math.random() * _VehicleType.length);
        const cars: Vehicle[] = [];
        const numCars = chance.integer({ min: 0, max: 3 });
        for (let j = 0; j < numCars; j++) {
            const vehicle = new Vehicle(
                `${chance.string({ alpha: true, length: 3 })} ${chance.string({ numeric: true, length: 3 })}`,
                user.id,
                '',
                _VehicleType[vehicleType],
            );
            allCars.push(vehicle);
            cars.push(vehicle);
        }

        const numTransactions = chance.integer({ min: 0, max: 30 });
        for (let j = 0; j < numTransactions && numCars > 0; j++) {
            const startDate = new Date(chance.date({ min: new Date('2020-01-01'), max: new Date() }));
            const transaction = new Transaction(
                startDate,
                chance.pickone(cars).plate,
                user.id,
                chance.pickone(parkingLots).id,
                _OSTypes[Math.round(Math.random())],
                alone,
            );
            const duration = chance.integer({ min: 0, max: 1440 });
            transaction.endTime = moment(startDate).add(duration, 'minutes').toDate();
            transaction.cost = duration * 300;
            transaction.state = TransactionState.COMPLETED;
            allTransactions.push(transaction);
        }
    }
    await db.save(allUsers);
    await db.save(allCars);
    await db.save(allTransactions);
};

export const randUniform = (min: number, max: number, step?: number) => {
    const n = Math.floor(Math.random() * (max - min + 1)) + min;
    return step !== undefined ? Math.round(n / step) * step : n;
};

export const randomParkingLot = (): ParkingLot => {
    const p = new ParkingLot(
        chance.company().slice(0, 50),
        'Voluptas placeat quia itaque consequatur reprehenderit sunt ipsa eligendi. Sed est pariatur consequatur voluptas sunt omnis non numquam veritatis. Praesentium est molestiae aut et. Quos nobis dolor enim est.',
        chance.longitude(),
        chance.latitude(),
        chance.address(),
    );
    p.id = chance.guid();
    return p;
};

export const randomTransaction = (userId: string, vehicles: Vehicle[]): Transaction => {
    const parkingLots = getMockParkingLots();
    const startDate = new Date(chance.date({ min: new Date('2020-01-01'), max: new Date() }));
    const transaction = new Transaction(
        startDate,
        chance.pickone(vehicles).plate,
        userId,
        chance.pickone(parkingLots).id,
        _OSTypes[Math.round(Math.random())],
    );
    const duration = chance.integer({ min: 0, max: 1440 });
    transaction.endTime = moment(startDate).add(duration, 'minutes').toDate();
    transaction.cost = duration * 300;
    transaction.state = TransactionState.COMPLETED;

    return transaction;
};
