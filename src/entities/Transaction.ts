import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { ParkingLot } from './ParkingLot';
import { User } from './User';
import { Vehicle } from './Vehicle';

export enum TransactionState {
    'WAITING' = 'WAITING',
    'REJECTED' = 'REJECTED',
    'COMPLETED' = 'COMPLETED',
    'ACTIVE' = 'ACTIVE',
}

@Entity()
export class Transaction {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    startTime: Date;

    @Column({ nullable: true })
    endTime?: Date;

    @Column()
    state: TransactionState;

    @Column({ nullable: true })
    cost?: number;

    @ManyToOne(() => User, (user) => user.transactions)
    @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
    user: User;
    @Index()
    @Column('uuid')
    user_id: string;

    @ManyToOne(() => Vehicle, (vehicle) => vehicle.transactions)
    @JoinColumn([
        { name: 'vehicle_plate', referencedColumnName: 'plate' },
        { name: 'user_id', referencedColumnName: 'user_id' },
    ])
    vehicle: Vehicle;

    @Column({ nullable: true })
    os: string;

    @Column({ nullable: true })
    alone: boolean;

    @Column()
    vehicle_plate: string;

    @ManyToOne(() => ParkingLot, (parkingLot) => parkingLot.transactions)
    @JoinColumn({ name: 'parking_lot_id' })
    parkingLot: ParkingLot;
    @Column('uuid')
    parking_lot_id: string;

    @UpdateDateColumn()
    updatedAt: Date;

    @CreateDateColumn()
    createdAt: Date;

    constructor(
        startTime: Date,
        vehiclePlate: string,
        userId: string,
        parking_lot_id: string,
        os: string,
        alone: boolean,
    ) {
        this.state = TransactionState.WAITING;
        this.vehicle_plate = vehiclePlate;
        this.startTime = startTime;
        this.user_id = userId;
        this.parking_lot_id = parking_lot_id;
        this.os = os;
        this.alone = alone;
        return this;
    }
}
