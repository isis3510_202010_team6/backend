import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Transaction } from './Transaction';
import { User } from './User';
import { ParkingLot } from './ParkingLot';

@Entity()
export class DistanceTravelled {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    distance: number;

    @Column({ nullable: true })
    user: string;

    @Column({ nullable: true })
    parkinglot: string;

    constructor(distance: number, user: string, parkingLot: string) {
        this.distance = distance;
        this.user = user;
        this.parkinglot = parkingLot;
        return this;
    }
}
