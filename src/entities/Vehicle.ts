import { Column, CreateDateColumn, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { Transaction } from './Transaction';
import { User } from './User';

export enum VehicleState {
    'ACTIVE' = 'ACTIVE',
    'INACTIVE' = 'INACTIVE',
    'DELETED' = 'DELETED',
}

@Entity()
@Index(['user', 'plate'], { unique: true })
export class Vehicle {
    @ManyToOne(() => User, (user) => user.vehicles)
    @JoinColumn({ name: 'user_id' })
    user: User;
    @PrimaryColumn('uuid')
    user_id: string;

    @PrimaryColumn({ type: 'varchar', length: 7 })
    plate: string;

    @Column()
    description: string;

    @Column()
    state: VehicleState;

    @OneToMany(() => Transaction, (transaction) => transaction.vehicle)
    transactions: Transaction[];

    @CreateDateColumn()
    createdAt: Date;

    @Column({ nullable: true })
    type: string;

    constructor(plate: string, user_id: string, description: string, type: string) {
        this.plate = plate;
        this.user_id = user_id;
        this.description = description;
        this.state = VehicleState.ACTIVE;
        this.type = type;
        return this;
    }
}
