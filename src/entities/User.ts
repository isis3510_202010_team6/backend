import { Chance } from 'chance';
import { Column, CreateDateColumn, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PaymentMethod } from './PaymentMethod';
import { Transaction } from './Transaction';
import { Vehicle } from './Vehicle';

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ length: 50 })
    name: string;

    @Index({ unique: true })
    @Column({ length: 50, unique: true })
    email: string;

    @Column({ nullable: true })
    phone?: string;

    @Column({ length: 100 })
    password: string;

    @Column({ length: 400, nullable: true })
    fcmToken: string;

    @OneToMany(() => Vehicle, (vehicle) => vehicle.user)
    vehicles: Vehicle[];

    @OneToMany(() => PaymentMethod, (paymentMethod) => paymentMethod.user)
    paymentMethods: PaymentMethod[];

    @OneToMany(() => Transaction, (transaction) => transaction.user)
    transactions: Transaction[];

    @Column({ nullable: true })
    address?: string;

    @Column({ nullable: true })
    job_type?: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column({ default: 'FALSE' })
    isFlutter: boolean;

    constructor(
        name: string,
        email: string,
        password: string,
        phone?: string,
        fcmToken?: string,
        isFlutter?: boolean,
        job_type?: string,
        address?: string,
    ) {
        this.id = new Chance().guid();
        this.email = email;
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.fcmToken = fcmToken;
        this.job_type = job_type;
        this.isFlutter = isFlutter !== undefined ? isFlutter : false;
        this.address = address;
        return this;
    }
}
