import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './User';

@Entity()
export class PaymentMethod {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Index()
    @Column('uuid')
    user_id: string;
    @ManyToOne(() => User, (user) => user.paymentMethods)
    @JoinColumn({ name: 'user_id' })
    user: User;

    @Column({ length: 100 })
    description: string;

    @Column({ length: 50 })
    provider: string;

    @Column({ length: 4 })
    lastDigits: string;

    constructor(lastDigits: string, provider: string, user_id: string, description: string) {
        this.lastDigits = lastDigits;
        this.provider = provider;
        this.user_id = user_id;
        this.description = description;
        return this;
    }
}
