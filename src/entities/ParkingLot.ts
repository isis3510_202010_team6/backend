import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Transaction } from './Transaction';

@Entity()
export class ParkingLot {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ length: 50 })
    name: string;

    @Column()
    address: string;

    @Column('double precision')
    latitud: number;

    @Column('double precision')
    longitud: number;

    @Column()
    description: string;

    @OneToMany(() => Transaction, (transaction) => transaction.vehicle)
    transactions: Transaction[];

    constructor(name: string, description: string, longitud: number, latitud: number, address: string) {
        this.name = name;
        this.description = description;
        this.longitud = longitud;
        this.latitud = latitud;
        this.address = address;
        return this;
    }
}
