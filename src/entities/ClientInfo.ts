import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ParkingLot } from './ParkingLot';

@Entity()
export class ClientInfo {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    job_type: string;

    @Column({ nullable: true })
    economicStanding: string;

    @Column({ nullable: true })
    numberOfCars: number;

    @Column({ nullable: true })
    age: number;

    @Column({ nullable: true })
    owner: boolean;

    constructor(job_type: string, economicStanding: string, numberOfCars: number, age: number, owner: boolean) {
        this.job_type = job_type;
        this.economicStanding = economicStanding;
        this.numberOfCars = numberOfCars;
        this.age = age;
        this.owner = owner;
        return this;
    }
}
