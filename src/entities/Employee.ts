import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ParkingLot } from './ParkingLot';

export enum ShiftType {
    'NIGHT' = 'NIGHT',
    'DAY' = 'DAY',
}

@Entity()
export class Employee {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    assignedParkingLot: string;

    @Column({ nullable: true })
    name: string;

    @Column({ nullable: true })
    shift_type: ShiftType;

    constructor(name: string, assignedParkingLot: string, shift_type: ShiftType) {
        this.name = name;
        this.assignedParkingLot = assignedParkingLot;
        this.shift_type = shift_type;
        return this;
    }
}
