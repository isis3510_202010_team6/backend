import { Column, Entity, OneToMany, Index, PrimaryGeneratedColumn } from 'typeorm';
import { Transaction } from './Transaction';
import { User } from './User';
import { ParkingLot } from './ParkingLot';

@Entity()
export class ConcurrentUsers {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    timestamp: string;

    @Column({ nullable: true })
    parkingLot: string;

    @Column({ nullable: true })
    amountOfUsers: number;

    constructor(timestamp: string, parkingLot: string, amountOfUsers: number) {
        this.timestamp = timestamp;
        this.parkingLot = parkingLot;
        this.amountOfUsers = amountOfUsers;
        return this;
    }
}
